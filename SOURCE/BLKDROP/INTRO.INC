{
    BSD 3-Clause License
    Copyright (c) 2021, Jerome Shidel
    All rights reserved.
}

function PlayImage(Filename : String) : boolean;
var
    LTT : longint;
    P  : PAsset;
    S : String;
    E : TEvent;
    TP : integer;
    SP, FP : TRGBPalettes;
    Abort : boolean;
    X : integer;
begin
    if not AssetLoad(Filename, asDefault, P) then begin
        PlayImage := true;
        exit;
    end;

    Abort := False;
    Video^.SetSync(true);
    PurgeEvents;
    Video^.GetPalettes(SP);
    if SlowVideoDriver then begin
        Video^.PutImage(
            PImage(P^.Data),
            GetMaxX div 2 - PImage(P^.Data)^.Width div 2,
            GetMaxY div 2 - PImage(P^.Data)^.Height div 2
        );
        for TP := 0 to 300 do begin
            { Video^.Shift(dmUp, 1, 0); }
            While (LTT = VRTimer^) do;
            LTT := VRTimer^;
            Video^.Update;
            GetEvent(E);
            if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then begin
                Abort := True;
                break;
            end;
        end;
    end
    else begin
        FillChar(FP, Sizeof(FP), 0);
        Video^.SetPalettes(FP);
        Video^.PutImage(
            PImage(P^.Data),
            GetMaxX div 2 - PImage(P^.Data)^.Width div 2,
            GetMaxY div 2 - PImage(P^.Data)^.Height div 2
        );
        if (Not Abort) then
            for TP := 0 to 50 do begin
                FP := SP;
                Video^.FadePalettes(FP, 100 - TP * 2);
                Video^.SetPalettes(FP);
                While (LTT = VRTimer^) do;
                LTT := VRTimer^;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then begin
                    Abort := True;
                    break;
                end;
            end;
        if (Not Abort) then
            for TP := 0 to 200 do begin
                { Video^.Shift(dmUp, 1, 0); }
                While (LTT = VRTimer^) do;
                LTT := VRTimer^;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then begin
                    Abort := True;
                    break;
                end;
            end;
        if (Not Abort) then
            for TP := 0 to 50 do begin
                { Video^.Shift(dmUp, 1, 0); }
                FP := SP;
                Video^.FadePalettes(FP, TP * 2);
                Video^.SetPalettes(FP);
                While (LTT = VRTimer^) do;
                LTT := VRTimer^;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then begin
                    Abort := True;
                    break;
                end;
            end;
    end;
    Video^.Fill(0);
    Video^.Update;
    if P^.MemSize > 0 then
        Video^.SetPalettes(SP);
    AssetUnlock(P);
    PlayImage := not Abort;
end;

function PlayText : boolean;
var
    LTT : word;
    FH, FX, I : integer;
    TP : word;
    P  : PAsset;
    BF : PFont;
    FS : TFontSettings;
    S : String;
    FC : TColor;
    E : TEvent;
    SP, FP : TRGBPalettes;
    Abort : boolean;
begin
    if SlowVideoDriver then exit;
    Video^.SetSync(True);
    PurgeEvents;
    GetFontSettings(FS);
    Video^.GetPalettes(SP);
    S := '1214N-' + Language;
    FontBestMatch(S, BF);
    if not AssetLoad('INTRO.' + Language, asDefault, P) then
        if not AssetLoad('INTRO.EN', asDefault, P) then exit;
    if P^.MemSize > 0 then begin
        TP := 0;
        FC := 7;
        LTT := VRTimer^;
        E.Kind := evNull;
        Abort := False;
        repeat
            S := '';
            while TP < P^.MemSize - 1 do begin
                if (Bytes(P^.Data^)[TP] = 10) or (Bytes(P^.Data^)[TP] = 0) then begin
                   { Ignore }
                end else
                if (Bytes(P^.Data^)[TP] = 13) then begin
                    Break;
                end else
                    S := S + Chars(P^.Data^)[TP];
                Inc(TP);
            end;
            Inc(TP);
            if (TP > P^.MemSize - 5) then begin
                if Assigned(BF) then Video^.SetFont(BF);
                FC := 12;
            end;
            FH := Video^.TextHeight(S);
            FX := GetMaxX div 2 - Video^.TextWidth(S) div 2;
            for I := 0 to FH - 1 do begin
                Video^.Shift(dmUp, 1, 0);
                Video^.PutText(FX, GetMaxY - I, S, FC);
                While (LTT = VRTimer^) do;
                LTT := VRTimer^;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then
                    Abort := True;
            end;
        until (TP >= P^.MemSize) or Abort;
        if (not Abort) then
            for TP := 0 to GetMaxY div 2 do begin
                Video^.Shift(dmUp, 1, 0);
                While (LTT = VRTimer^) do;
                LTT := VRTimer^;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then begin
                    Abort := True;
                    break;
                end;
            end;
        if (Not Abort) then
            for TP := 0 to 33 do begin
                Video^.Shift(dmUp, 1, 0);
                FP := SP;
                Video^.FadePalettes(FP, TP * 3);
                Video^.SetPalettes(FP);
                While (LTT = VRTimer^) do;
                LTT := VRTimer^;
                Video^.Update;
                GetEvent(E);
                if (E.Kind = evKeyPress) or (E.Kind = evMouseClick) then begin
                    Abort := True;
                    break;
                end;
            end;
    end;
    Video^.Fill(0);
    Video^.Update;
    Video^.SetPalettes(SP);
    SetFontSettings(FS);
    AssetUnlock(P);
    PlayText := not Abort;
end;

procedure PlayIntro;
begin
    SetVRTInterval(4);
    PlayImage('SPLAT160.IGG');
    PlayImage('POFD160.IGG');
end;

procedure PlayExtro;
begin
    SetVRTInterval(3);
	Video^.Fill(0);
    PlayImage('PBDE160.IGG');
end;
